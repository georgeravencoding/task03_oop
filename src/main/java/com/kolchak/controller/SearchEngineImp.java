package com.kolchak.controller;

import com.kolchak.model.*;

import java.util.ArrayList;
import java.util.Scanner;

public class SearchEngineImp implements SearchEngine {
    Scanner scanner = new Scanner(System.in);

    private Home apart1 = new Apartment("Lviv", false, 4, 77.5, false, true,
            1, 1, new GovernmentInstitutionLviv(13.4, 1.3, 3.9));
    private Home apart2 = new Apartment("Lviv", true, 4, 77.5, false, false,
            2, 3, new GovernmentInstitutionLviv(15.2, 15.6, 2.5));
    private Home penth1 = new Penthouse("Lviv", false, 4, 157.5, true, true,
            3, 12.4, new GovernmentInstitutionLviv(2.1, 5.2, 4.6));
    private Home penth2 = new Penthouse("Lviv", true, 4, 121.9, true, true,
            4, 16.6, new GovernmentInstitutionLviv(10.2, 2.7, 3.4));
    private Home house1 = new House("Lviv", true, 8, 260.5, false, true,
            5, 24.8, new GovernmentInstitutionLviv(23.9, 1.2, 36.8));
    private Home house2 = new House("Lviv", false, 8, 197.5, true, false,
            6, 34.4, new GovernmentInstitutionLviv(4.3, 2.5, 3.5));

    private ArrayList<Home> listOfHomes() {
        ArrayList<Home> homes = new ArrayList<>();
        homes.add(apart1);
        homes.add(apart2);
        homes.add(penth1);
        homes.add(penth2);
        homes.add(house1);
        homes.add(house2);
        return homes;
    }

    public void showListHome() {
        for (Home home : listOfHomes()) {
            System.out.println(home);
        }
    }

    public void searchByFloor() {
        System.out.print("Input floor level: ");
        int floorInput = scanner.nextInt();
        for (Home home : listOfHomes()) {
            if (home.getFloor() == floorInput) {
                System.out.println(home);
            }
        }
    }

    public void searchByMaxRentPrice() {
        System.out.print("Input max rent price: ");
        int maxRentPriceInput = scanner.nextInt();
        for (Home home : listOfHomes()) {
            if (home.getRentPrice() < maxRentPriceInput) {
                System.out.println(home);
            }
        }
    }

    public void searchByMinAndMaxRentPrice() {
        System.out.print("Input min rent price: ");
        int minRentPrice = scanner.nextInt();
        System.out.print("Input max rent price: ");
        int maxRentPrice = scanner.nextInt();
        for (Home home : listOfHomes()) {
            if (home.getRentPrice() > minRentPrice && home.getRentPrice() < maxRentPrice) {
                System.out.println(home);
            }
        }
    }

    public void searchByMaxDistanceToSchool() {
        System.out.print("Input max distance to school: ");
        double distanceInput = scanner.nextDouble();
        for (Home home : listOfHomes()) {
            if (home.governmentInstitutionLviv.getSchoolDistance() < distanceInput) {
                System.out.println(home);
            }
        }
    }
}
