package com.kolchak.controller;

public interface SearchEngine {
    void showListHome();

    void searchByFloor();

    void searchByMaxRentPrice();

    void searchByMinAndMaxRentPrice();

    void searchByMaxDistanceToSchool();
}
