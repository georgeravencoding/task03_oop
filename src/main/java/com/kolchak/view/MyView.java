package com.kolchak.view;

import com.kolchak.controller.SearchEngine;
import com.kolchak.controller.SearchEngineImp;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private SearchEngine searchEngine;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        searchEngine = new SearchEngineImp();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print list of homes");
        menu.put("2", "  2 - search home by floor level");
        menu.put("3", "  3 - search home by max rent price");
        menu.put("4", "  4 - search home by min and max rent price");
        menu.put("5", "  5 - search home by max distance to school");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    private void pressButton1() {
        searchEngine.showListHome();
    }

    private void pressButton2() {
        searchEngine.searchByFloor();
    }

    private void pressButton3() {
        searchEngine.searchByMaxRentPrice();
    }

    private void pressButton4() {
        searchEngine.searchByMinAndMaxRentPrice();
    }

    private void pressButton5() {
        searchEngine.searchByMaxDistanceToSchool();
    }

    private void inputMenu() {
        System.out.println("MENU");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            inputMenu();
            System.out.println("Please select menu point: ");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {

            }
        } while (!keyMenu.equalsIgnoreCase("Q"));
    }
}

