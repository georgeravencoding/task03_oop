package com.kolchak.model;

public class Apartment extends Home {
    private int balconyAmount;

    public Apartment(String location, boolean isLuxury, int roomAmount,
                     double squareArea, boolean isFurnitured,
                     boolean goodNeighbours, int floor, int balconyAmount,
                     GovernmentInstitutionLviv governmentInstitutionLviv) {
        super(location, isLuxury, roomAmount, squareArea, isFurnitured,
                goodNeighbours, floor, governmentInstitutionLviv);
        this.balconyAmount = balconyAmount;
    }

    public int getBalconyAmount() {
        return balconyAmount;
    }

    public void setBalconyAmount(int balconyAmount) {
        this.balconyAmount = balconyAmount;
    }

    @Override
    public String toString() {
        return "This " + type.toLowerCase() + " located in " + location
                + ". It is on " + floor + " floor."
                + "\nIt`s have " + roomAmount + " room(s), also " + balconyAmount
                + " balcony(es)."
                + "\nTotal area is " + squareArea + " meters square."
                + super.toString()
                + governmentInstitutionLviv.toString()
                + "\nTotal cost of rent per month: " + rentPrice
                + "\n==============================================";
    }

}

