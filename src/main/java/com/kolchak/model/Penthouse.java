package com.kolchak.model;

public class Penthouse extends Home {
    private double terraceSquare;

    public Penthouse(String location, boolean isLuxury, int roomAmount,
                     double squareArea, boolean isFurnitured,
                     boolean goodNeighbours, int floor, double terraceSquare,
                     GovernmentInstitutionLviv governmentInstitutionLviv) {
        super(location, isLuxury, roomAmount, squareArea, isFurnitured,
                goodNeighbours, floor, governmentInstitutionLviv);
        this.terraceSquare = terraceSquare;
    }

    public double isTerraceSquare() {
        return terraceSquare;
    }

    public void setTerraceSquare(double terraceSquare) {
        this.terraceSquare = terraceSquare;
    }

    @Override
    public String toString() {
        return "This " + type.toLowerCase() + " located in " + location
                + ". It is on " + floor + " floor."
                + "\nIt`s have " + roomAmount + " room(s), also terrace with "
                + terraceSquare + " meters square. "
                + "\nTotal area is " + squareArea + " meters square.\n"
                + super.toString()
                + governmentInstitutionLviv.toString()
                + "\nTotal cost of rent per month: " + rentPrice
                + "\n==============================================";
    }

}
