package com.kolchak.model;

public class House extends Home {
    private double territorySquare;

    public House(String location, boolean isLuxury, int roomAmount,
                 double squareArea, boolean isFurnitured,
                 boolean goodNeighbours, int floor, double terraceSquare,
                 GovernmentInstitutionLviv governmentInstitutionLviv) {
        super(location, isLuxury, roomAmount, squareArea, isFurnitured,
                goodNeighbours, floor, governmentInstitutionLviv);
        this.territorySquare = terraceSquare;
    }

    public double isTerraceSquare() {
        return territorySquare;
    }

    public void setTerraceSquare(double terraceSquare) {
        this.territorySquare = terraceSquare;
    }

    @Override
    public String toString() {
        return "This " + type.toLowerCase() + " located in " + location
                + ". It is on " + floor + " floor."
                + "\nIt`s have " + roomAmount + " room(s), also territory outside"
                + " the house with " + territorySquare + " meters square. "
                + "\nTotal area is " + squareArea + " meters square.\n"
                + super.toString()
                + governmentInstitutionLviv.toString()
                + "\nTotal cost of rent per month: " + rentPrice
                + "\n==============================================";
    }
}
