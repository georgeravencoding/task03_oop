package com.kolchak.model;

public class Home {

    protected String location;
    protected String type = getClass().getSimpleName();
    protected boolean isLuxury;
    protected int roomAmount;
    protected double squareArea;
    protected boolean isFurnitured;
    protected int rentPrice;
    protected boolean goodNeighbours;
    protected int floor;
    public GovernmentInstitutionLviv governmentInstitutionLviv;

    protected Home(String location, boolean isLuxury, int roomAmount,
                   double squareArea, boolean isFurnitured,
                   boolean goodNeighbours, int floor,
                   GovernmentInstitutionLviv governmentInstitutionLviv) {
        this.location = location;
        this.isLuxury = isLuxury;
        this.roomAmount = roomAmount;
        this.squareArea = squareArea;
        this.isFurnitured = isFurnitured;
        this.goodNeighbours = goodNeighbours;
        this.floor = floor;
        this.rentPrice = calculatePrice();
        this.governmentInstitutionLviv = governmentInstitutionLviv;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isLuxury() {
        return isLuxury;
    }

    public void setLuxury(boolean luxury) {
        isLuxury = luxury;
    }

    public int getRoomAmount() {
        return roomAmount;
    }

    public void setRoomAmount(int roomAmount) {
        this.roomAmount = roomAmount;
    }

    public double getSquareArea() {
        return squareArea;
    }

    public void setSquareArea(double squareArea) {
        this.squareArea = squareArea;
    }

    public boolean isFurnitured() {
        return isFurnitured;
    }

    public void setFurnitured(boolean furnitured) {
        isFurnitured = furnitured;
    }

    public boolean isGoodNeighbours() {
        return goodNeighbours;
    }

    public void setGoodNeighbours(boolean goodNeighbours) {
        this.goodNeighbours = goodNeighbours;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getRentPrice() {
        return rentPrice;
    }

    private int calculatePrice() {
        int result = 0;
        if (location == "Lviv") {
            result += PriceList.LOCATION_LVIV.getPriceForFeatures();
        }
        if (type.equalsIgnoreCase("Apartment")) {
            result += PriceList.TYPE_APARTMENT.getPriceForFeatures();
        }
        if (type.equalsIgnoreCase("House")) {
            result += PriceList.TYPE_HOUSE.getPriceForFeatures();
        }
        if (type.equalsIgnoreCase("Penthouse")) {
            result += PriceList.TYPE_PENTHOUSE.getPriceForFeatures();
        }
        if (isLuxury) {
            result += PriceList.LUXURY.getPriceForFeatures();
        }
        result += roomAmount * PriceList.PER_ROOM.getPriceForFeatures();

        result += (int) (squareArea * PriceList.SQUARE_PER_METER
                .getPriceForFeatures());

        if (isFurnitured) {
            result += PriceList.FURNITURE.getPriceForFeatures();
        }
        if (goodNeighbours) {
            result += PriceList.NEIGHBOURS.getPriceForFeatures();
        }
        if (floor == 1) {
            result += PriceList.FLOOR_1.getPriceForFeatures();
        }
        if (floor == 2) {
            result += PriceList.FLOOR_2.getPriceForFeatures();
        }
        if (floor == 3) {
            result += PriceList.FLOOR_3.getPriceForFeatures();
        }
        if (floor == 4) {
            result += PriceList.FLOOR_4.getPriceForFeatures();
        }
        return rentPrice += result;
    }

    @Override
    public String toString() {
        String aboutNeighbours = goodNeighbours
                ? "\nNeighbours are very good people. \n"
                : "Neighbours are really bad people. \n";
        String aboutLuxury = isLuxury ? "This is very luxury place. \n"
                : "This is awful place. \n";
        String aboutFurniture = isFurnitured ? "All furniture is present. \n"
                : "There is no any furniture inside. \n";
        String aboutPrice = rentPrice > 3000 ? "It is pretty expensive "
                + type.toLowerCase() + ".\n" : "It is very cheap "
                + type.toLowerCase() + ".\n";
        ;
        return aboutNeighbours + aboutLuxury + aboutFurniture + aboutPrice;
    }
}
