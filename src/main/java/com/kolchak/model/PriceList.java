package com.kolchak.model;

public enum PriceList {
    LOCATION_LVIV(1000),
    TYPE_APARTMENT(600),
    TYPE_HOUSE(600),
    TYPE_PENTHOUSE(600),
    LUXURY(200),
    PER_ROOM(1),
    SQUARE_PER_METER(20),
    FURNITURE(100),
    NEIGHBOURS(200),
    FLOOR_1(200),
    FLOOR_2(100),
    FLOOR_3(100),
    FLOOR_4(400);
    int priceForFeatures;

    PriceList(int priceForFeatures) {
        this.priceForFeatures = priceForFeatures;
    }

    public int getPriceForFeatures() {
        return priceForFeatures;
    }

    public void setPriceForFeatures(int priceForFeatures) {
        this.priceForFeatures = priceForFeatures;
    }
}
