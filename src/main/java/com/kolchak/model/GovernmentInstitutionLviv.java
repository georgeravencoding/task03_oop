package com.kolchak.model;

public class GovernmentInstitutionLviv {
    private double hospitalDistance;
    private double schoolDistance;
    private double policeDistance;

    public GovernmentInstitutionLviv(double hospitalDistance,
                                     double schoolDistance,
                                     double policeDistance) {
        this.hospitalDistance = hospitalDistance;
        this.schoolDistance = schoolDistance;
        this.policeDistance = policeDistance;
    }

    public double getHospitalDistance() {
        return hospitalDistance;
    }

    public void setHospitalDistance(double hospitalDistance) {
        this.hospitalDistance = hospitalDistance;
    }

    public double getSchoolDistance() {
        return schoolDistance;
    }

    public void setSchoolDistance(double schoolDistance) {
        this.schoolDistance = schoolDistance;
    }

    public double getPoliceDistance() {
        return policeDistance;
    }

    public void setPoliceDistance(double policeDistance) {
        this.policeDistance = policeDistance;
    }

    @Override
    public String toString() {
        return "\nLoading distances to Government Institutions..."
                + "\nDistance to hospital: " + hospitalDistance + " km"
                + "\nDistance to school: " + schoolDistance + " km"
                + "\nDistance to police: " + policeDistance + " km\n";
    }
}
