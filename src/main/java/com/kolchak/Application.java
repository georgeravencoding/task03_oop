package com.kolchak;

import com.kolchak.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}